<?php

namespace Drupal\soauth\Error\OAuth;

use Drupal\soauth\Error\SoAuthError;


/**
 * Class OAuthError
 * @author Raman Liubimau <raman@cmstuning.net>
 */
class OAuthError extends SoAuthError {
   
}
